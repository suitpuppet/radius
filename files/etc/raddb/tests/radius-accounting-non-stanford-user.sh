#!/bin/sh

# Function to generate a unique Acct-Session-Id
generate_session_id() {
    hexdump -n 8 -e '8/1 "%02X" "\n"' /dev/urandom
}

# Function to send RADIUS accounting packet
send_packet() {
    local type=$1
    local session_id=$2
    local radclient_ip="127.0.0.1:18127"
    local radclient_secret="testing123"
    local temp_file="/tmp/radius_packet"

    cat <<EOF > $temp_file
Acct-Status-Type = $type
Acct-Session-Id = $session_id
User-Name = "leland@mit.edu"
NAS-IP-Address = "127.0.0.1"
NAS-Port = 0
Calling-Station-Id = "00-00-00-00-00-00"
Called-Station-Id = "00-00-00-00-00-00"
EOF

    if [ $type != "Start" ]; then
        echo "Framed-Ip-Address = 127.0.0.2" >> $temp_file
    fi

    radclient -x -F -f $temp_file $radclient_ip acct $radclient_secret
}

# Generate a unique Acct-Session-Id for the session
session_id=$(generate_session_id)

# Send Start packet
echo "Sending Start packet..."
send_packet "Start" "$session_id"

# Wait for some time before sending Interim-Update packet (adjust sleep duration as needed)
sleep 5

# Send Interim-Update packet
echo "Sending Interim-Update packet..."
send_packet "Interim-Update" "$session_id"

# Wait for some time before sending Stop packet (adjust sleep duration as needed)
sleep 5

# Send Stop packet
echo "Sending Stop packet..."
send_packet "Stop" "$session_id"

# Clean up
rm -f /tmp/radius_packet

echo "Done."
