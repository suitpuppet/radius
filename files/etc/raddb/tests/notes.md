# Tests

```
docker run -it --rm --name radius-test --network host -v /etc/raddb:/etc/raddb:ro -v /tmp/tests:/tests us-docker.pkg.dev/uit-authnz/docker-public/radius-radiusd:latest /bin/sh
```

## WiFi with EAP-TTLS + MSCHAPv2

Copy `eap-ttls-*.conf` and replace PASSWORD with `idpsso1`'s pasword, then run:

```
  eapol_test -c eap-ttls-mschapv2.conf      -a 127.0.0.1 -p 18126 -s testing123
  eapol_test -c eap-ttls-mschapv2-anon.conf -a 127.0.0.1 -p 18126 -s testing123
```

## WiFi with EAP-PEAP + MSCHAPv2

Copy `eap-peap-*.conf` and replace PASSWORD with `idpsso1`'s pasword, then run:

```
  eapol_test -c eap-peap-mschapv2.conf      -a 127.0.0.1 -p 18126 -s testing123
  eapol_test -c eap-peap-mschapv2-anon.conf -a 127.0.0.1 -p 18126 -s testing123
```

## Wifi with EAP-TLS + Cardinal Key

Run:

```
  eapol_test -c eap-tls.conf      -a 127.0.0.1 -p 18126 -s testing123
  eapol_test -c eap-tls-anon.conf -a 127.0.0.1 -p 18126 -s testing123
```


## VPN with Cardinal Key

Copy `test-ck.radclient` and update `User-Name` and `User-Password` to the `idpsso1/Entitlement-...` of the test cert, then run

```
  radclient -f test-ck.radclient 127.0.0.1:18124 auth testing123
```

## VPN with Username, Password, and Duo

TODO: more work

Using `idpsso1`'s password, run

  radtest -t pap -x idpsso1 PASSWORD 127.0.0.1:18124 0 testing123
```

You should get an error that `Access-Challenge` was received instead of `Access-Accept`.

For now, the error is `Access-Reject`, since `idpsso1` is disabled in Duo.

