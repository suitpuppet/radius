#! /bin/bash

TMPDIR=$(mktemp -d /tmp/convert-clients-XXXXXX)

GWO=${TMPDIR}/get-wallet-objects.sh

cat >${GWO} <<EOF_GWO
#! /bin/bash

export KRB5CCNAME=FILE:/tmp/swl.root
wallet_get_file () {
  remctl wallet.stanford.edu wallet get file \$1 >\$2
}

EOF_GWO

awkscript=$(cat <<AWK
BEGIN { f = ""; } \
/proxy_fragment/ { gsub("[:'{]", "", \$2); f = \$2; } \
/desc =>/        { \
  gsub("[,']", "", \$3);  \
  printf("echo Getting %s from wallet as ${TMPDIR}/%s.orig\\n", f, \$3); \
  printf("wallet_get_file %s ${TMPDIR}/%s.orig\\n", f, \$3); \
  f = ""; \
}
AWK
)

awk "${awkscript}" $1 >> ${GWO}
chmod +x ${GWO}

${GWO}

mkdir -p ${TMPDIR}/orig
for o in ${TMPDIR}/*.orig; do
  n=$(basename $o .orig)
  c=${TMPDIR}/${n}
  echo Converting $o to client $n
  sed -E \
      -e '/^#? *$/d' \
      -e 's/^radius_ip_[0-9][0-9]* *= *([^ ]*)$/\nclient '${n}' {\n  shortname      = '${n}'\n  ipaddr         = \1/' \
      -e 's/^radius_secret_[0-9][0-9]* *= *([^ ]*)$/  secret         = \1\n  virtual_server = duoproxy\n}/' \
      $o > ${c}
  mv $o ${TMPDIR}/orig
  grep -q '^ *ipaddr *= *.*-' ${c} && echo '#TODO convert IP range to valid CIDR range' >>${c}
done

for c in ${TMPDIR}/*; do
 [ -f $c ] && [ ! -x $c ] && grep -H '^#TODO' $c
done
