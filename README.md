# RADIUS

Puppet module to configure FreeRADIUS, Duo Authentication Proxy, and Samba / Winbind for a UIT-ET-ACS consolidated RADIUS server. The services run in Docker containers, and the configuration is mounted into those containers from the host filesystem.

The module is available from [code.stanford.edu](https://code.stanford.edu/suitpuppet/radius)

## Container Images

| Service          | Image           | Repo                                                    |
|------------------|-----------------|---------------------------------------------------------|
| service/radiusd  | radius-radiusd  | https://code.stanford.edu/authnz/docker-radius-radiusd  |
| service/samba    | radius-samba    | https://code.stanford.edu/authnz/docker-radius-samba    |
| service/duoproxy | radius-duoproxy | https://code.stanford.edu/authnz/docker-radius-duoproxy |

### Future Images / Services

| Service          | Image       | Repo                                                |
|------------------|-------------|-----------------------------------------------------|
| service/splunkuf | splunkuf    | https://code.stanford.edu/authnz/docker-splunkuf    |
| service/vpntest  | radius-test | https://code.stanford.edu/authnz/docker-radius-test |
| service/eaptest  | radius-test | https://code.stanford.edu/authnz/docker-radius-test |

## Ports and Sockets

### Open to World

| Port | Proto | Container | Use                    |
|------|-------|-----------|------------------------|
| 1812 | UDP   | radiusd   | Authorization listener |
| 1813 | UDP   | radiusd   | Accounting listener    |

#### Future

| Port | Proto | Container | Use                    |
|------|-------|-----------|------------------------|
| 1812 | TCP   | radiusd   | Authorization listener |
| 1812 | TCP6  | radiusd   | Authorization listener |
| 1812 | UDP6  | radiusd   | Authorization listener |
| 1813 | TCP6  | radiusd   | Accounting listener    |
| 1813 | TCP6  | radiusd   | Accounting listener    |
| 1813 | UDP6  | radiusd   | Accounting listener    |
| 2083 | TCP   | radiusd   | RADSEC listener        |
| 2083 | TCP6  | radiusd   | RADSEC listener        |

### Open to Campus

RFC1918 addresses, and some sub-ranges from 171.64.0.0/14.

| Port | Proto | Container | Use                                                     |
|------|-------|-----------|---------------------------------------------------------|
| 8081 | TCP   | duoproxy  | HTTP Proxy for shadow-net1                              |
| 8082 | TCP   | duoproxy  | HTTP Proxy for ISO's Duo instance                       |
| 8083 | TCP   | duoproxy  | HTTP Proxy for shadow-net2                              |
| 8084 | TCP   | duoproxy  | HTTP Proxy for it.win.stanford.edu and win.stanford.edu |


### Localhost (127.0.0.1) Only

| Port  | Proto | Container | Use                                  |
|-------|-------|-----------|--------------------------------------|
| 18120 | UDP   | radiusd   | inner-tunnel listener (for EAP-TTLS) |
| 18121 | UDP   | radiusd   | status server listener               |
| 18122 | UDP   | duoproxy  | Duo Authorization listener           |
| 18124 | UDP   | radiusd   | VPN Authorization test listener      |
| 18125 | UDP   | radiusd   | VPN Accounting test listener         |
| 18126 | UDP   | radiusd   | eduroam Authorization test listener  |
| 18127 | UDP   | radiusd   | eduroam Accounting test listener     |


### Unix Domain Sockets

| Path                                 | Container | Use                         |
|--------------------------------------|-----------|-----------------------------|
| `/var/lib/samba/winbindd_privileged` | radiusd   | `winbind` volume            |
| `/var/lib/samba/winbindd_privileged` | samba     | `winbind` volume            |
| `/var/run/radiusd/radiusd.sock`      | radiusd   | control socket for `radmin` |

## Hiera Data

Configuration data is stored on the puppet master in `hdata/su_group/radius-prod.yaml` (production) and `hdata/su_group/radius-dev.yaml` (dev).

Production data looks like:

```yaml
---
# su_group (radius-prod) hiera settings
#
# Applies to all servers with the su_group fact 'radius-prod'

## radius settings
radius::env: prod
radius::docker_base: us-docker.pkg.dev/uit-authnz/docker-public
radius::wallet_base: config/its-idg/radius

radius::freeradius::uid: 0
radius::freeradius::gid: 95
radius::client::gid: 95
radius::proxy::gid: 95
radius::freeradius::backup_logs: true
radius::freeradius::enable_lbcd: true

radius::freeradius::ldap_server: ldap.stanford.edu
radius::freeradius::ldap_port: 389
radius::freeradius::ldap_base_dn: cn=accounts,dc=stanford,dc=edu
radius::freeradius::ldap_realm: stanford.edu
radius::freeradius::ldap_acct_status: suAccountStatus
radius::freeradius::ldap_kerb_status: suKerberosStatus
radius::freeradius::ldap_priv_group: suPrivilegeGroup

radius::freeradius::mysql_server: 10.10.10.10
radius::freeradius::mysql_port: 3306
radius::freeradius::mysql_login: radius_prod
radius::freeradius::mysql_db: certcache_prod

## Proxy servers (config from wallet)
radius::freeradius::proxies:
  - duoproxy
  - eduroam
  - slac

## RADIUS clients (config from wallet)
radius::freeradius::clients:
  - dorm-wireless-controllers
  - duoproxy
  - eduroam
  - mist-aps
  - networking-paloalto-vpn
  - networking-vpn
  - slac
  - testing
  - vpn
  - vpn-lab
  - wireless-controllers

## FreeRADIUS modules to enable
radius::freeradius::mods:
  - always
  - attr_filter
  - cache_account_status
  - cache_authmethod
  - cache_eap
  - cache_sunac
  - cache_vpn_pool
  - date
  - detail
  - detail.log
  - eap
  - exec
  - expr
  - files
  - inner-eap
  - krb5
  - ldap
  - linelog
  - mschap
  - ntlm_auth
  - pap
  - preprocess
  - sql
  - utf8

## FreeRADIUS sites to enable
radius::freeradius::sites:
  - cardinalkey
  - control-socket
  - default
  - duo_authn
  - eduroam
  - inner-tunnel
  - status
  - vpn

radius::samba::uid: 0
radius::samba::gid: 0

radius::duoproxy::uid: 0
radius::duoproxy::gid: 0
radius::duoproxy::http_proxies:
  proxy1:
    duo_server: api-XXXXXXXX.duosecurity.com
    port: 8081
    iptables:
      - 10.0.0.0/8         # Private /8
      - 172.16.0.0/12      # Private /12
      - 192.168.0.0/16     # Private /16
    description: |-
      Connect to api-XXXXXXXX.duosecurity.com from all RFC1918 addresses.


user::root::principals:
  - alice/root@stanford.edu
  - bob/root@stanford.edu

su_local_root::accounts::users:
  alice:
    - 12345
    - bash
  bob:
    - 23456
    - bash

## Defaults
defaults:
  base:
    use_ossec: true
    use_tripwire: false
    sunetids:
      - alice
      - bob

# Base
base:
  ntp:
    allow_ipv4_only: true
```

## Wallet data

### Duo Proxy

| Filename               | Wallet File Path                          |
|------------------------|-------------------------------------------|
| /etc/duoproxy/base.cfg | config/its-idg/radius/authproxy-cfg-_ENV_ |


### FreeRADIUS

| Filename                   | Wallet File Path                    |
|----------------------------|-------------------------------------|
| /etc/raddb/local.d/secrets | config/its-idg/radius/secrets-_ENV_ |


### Samba

| Filename                                        | Wallet File Path                                                   |
|-------------------------------------------------|--------------------------------------------------------------------|
| /var/lib/samba/private/secrets.tdb              | config/its-idg/radius/samba-secrets-tdb-_RADIUS-SERVER_            |
| /var/lib/samba/private/netlogon\_creds\_cli.tdb | config/its-idg/radius/samba-netlogon-creds-cli-tdb-_RADIUS-SERVER_ |

**NOTE**: We also used to store `/etc/samba/keytab` and `/var/lib/samba/private/secrets.ldb` in wallet, but those do not appear to be required for Samba to function.

# Future Ideas

# Notes

## Server configuration sections

* accounting
* authorize
* authenticate
* client
* listen
* post-auth
  * Post-Auth-Type Challenge
  * Post-Auth-Type Client-Lost
  * Post-Auth-Type Reject
  * Post-Auth-Type Reject
* post-proxy
  * Post-Proxy-Type Fail-Accounting
  * Post-Proxy-Type Fail-Authentication
  * Post-Proxy-Type Fail-CoA
  * Post-Proxy-Type Fail-Disconnect
* preacct
* pre-proxy
* session

# Footnotes

1. The DUO HTTP Proxy is just a simple HTTP proxy, relying on clients to use the CONNECT method. We could replace it with Apache, NGINX, or HAProxy without much difficulty.
