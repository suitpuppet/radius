# Create a Duo HTTP Proxy from Hiera data

define radius::duoproxy::httpproxy (
  $duo_dir      = '/etc/duoproxy',
  $base_cfg     = "${duo_dir}/base.cfg",
  $duo_server   = undef,
  $port         = undef,
  $description  = '',
  $iptables     = [],
)
{
  if (!$duo_server) {
    fail 'missing required duo_server parameter'
  }

  if (!$port) {
    fail 'missing required port parameter'
  }

  $path = "${duo_dir}/http-proxy-${port}-${name}"

  file { $path :
    ensure  => present,
    content => template("${module_name}/etc/duoproxy/http_proxy.erb"),
  }

  # Create iptables rules file fragments
  base::iptables::rule { "http-proxy-${port}-${name}":
    ensure      => 'present',
    description => "Allow http-proxying to ${duo_server} from these address ranges",
    source      => $iptables,
    port        => [ $port ],
    protocol    => 'tcp',
  }

}
