# Add script and cron entry to backup logs to a GCP bucket

class radius::log_backup (
  $script_name   = 'radius-log-backup',
  $wallet_object = 'config/its-idg/radius-log/gcp-user',
  $base_dir      = '/usr/local',
  $bin_dir       = "${base_dir}/bin",
  $key_dir       = "${base_dir}/etc",
  $key_file      = "${key_dir}/gcp-user.json",
  $log_base      = '/var/log/radius',
  $cron_dir      = '/etc/cron.d',
  $gcp_bucket    = 'gs://prod-uit-authnz-radius-backup',
){

  include google_cloud_sdk_package

  # Make a directory to hold backup.sh and gcp credentials json file

  file {
    [
      $base_dir,
      $bin_dir,
      $cron_dir,
      $key_dir
    ]:
      ensure => directory,
      owner  => 0,
      group  => 0,
      mode   => '0755',
  }

  file { "${bin_dir}/${script_name}":
    ensure  => present,
    owner   => 0,
    group   => 0,
    mode    => '0755',
    content => template("${module_name}/bin/${script_name}.erb"),
    require => File[$bin_dir],
  }

  base::wallet { $wallet_object:
    path    => "${key_file}",
    type    => 'file',
    owner   => 0,
    group   => 0,
    mode    => '0600',
    require => File[$key_dir],
  }

  file { "${cron_dir}/${script_name}":
    ensure  => present,
    owner   => 0,
    group   => 0,
    mode    => '0644',
    content => template("${module_name}/etc/cron.d/${script_name}.erb"),
    require => File[$cron_dir],
  }

}
