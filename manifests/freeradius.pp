# FreeRADIUS server configuration

class radius::freeradius (
  $env              = $radius::env,
  $svc_princ        = 'service/radius-cert',
  $docker_base      = 'us-docker.pkg.dev/uit-authnz/docker-public',
  $docker_tag       = 'latest',
  $wallet_base      = 'config/its-idg/radius',

  $uid              = 95,
  $gid              = 95,

  $raddb            = '/etc/raddb',
  $enable_radsec    = false,

  $mods             = [],
  $sites            = [],
  $clients          = [],
  $proxies          = [],

  $ldap_server      = 'ldap.stanford.edu',
  $ldap_port        = 389,
  $ldap_base_dn     = 'cn=accounts,dc=stanford,dc=edu',
  $ldap_realm       = 'stanford.edu',
  $ldap_acct_status = 'suAccountStatus',
  $ldap_kerb_status = 'suKerberosStatus',
  $ldap_priv_group  = 'suPrivilegeGroup',

  $mysql_server     = undef,
  $mysql_port       = 3306,
  $mysql_login      = undef,
  $mysql_db         = undef,

  $backup_logs      = true,

) {

  if (!$mysql_server) {
    fail 'missing required mysql_server parameter'
  }

  if (!$mysql_login) {
    fail 'missing required mysql_login parameter'
  }

  if (!$mysql_db) {
    fail 'missing required mysql_db parameter'
  }

  file { '/var/log/radius':
    ensure => directory,
    owner  => $uid,
    group  => $gid,
    mode   => '0775'
  }

  #
  # copy static configuration
  #

  file { $raddb:
    ensure  => present,
    owner   => 0,
    group   => $gid,
    mode    => 'u+rwX,g+rX,o+rX',
    source  => "puppet:///modules/${module_name}/etc/raddb",
    recurse => true,
    purge   => false,
  }

  #
  # Create cert hashes
  #

  exec { 'c_rehash':
    command => "/usr/bin/c_rehash ${raddb}/certs",
    group   => $gid,
    unless  => 'test -n "`find /etc/raddb/certs/ -type l -group root`"',
    require => File[$raddb],
  }

  # Enable clients

  $clients.each | String $client | {
    file { "${raddb}/clients-enabled/${client}":
      ensure  => link,
      owner   => 0,
      group   => $gid,
      mode    => '0644',
      target  => "${raddb}/clients-available/${client}",
      require => File[ $raddb ],
    }
  }

  # Enable modules

  $mods.each | String $mod | {
    file { "${raddb}/mods-enabled/${mod}":
      ensure  => link,
      owner   => 0,
      group   => $gid,
      mode    => '0644',
      target  => "${raddb}/mods-available/${mod}",
      require => File[ $raddb ],
    }
  }

  # Enable proxies

  $proxies.each | String $proxy | {
    file { "${raddb}/proxies-enabled/${proxy}":
      ensure  => link,
      owner   => 0,
      group   => $gid,
      mode    => '0644',
      target  => "${raddb}/proxies-available/${proxy}",
      require => File[ $raddb ],
    }
  }

  # Enable sites

  $sites.each | String $site | {
    file { "${raddb}/sites-enabled/${site}":
      ensure  => link,
      owner   => 0,
      group   => $gid,
      mode    => '0644',
      target  => "${raddb}/sites-available/${site}",
      require => File[ $raddb ],
    }
  }


  #
  # get secrets
  #
  base::wallet { "${wallet_base}/secrets-${env}":
    path    => "${raddb}/local.d/secrets",
    type    => 'file',
    owner   => 0,
    group   => $gid,
    mode    => '0640',
    require => File[ $raddb ],
  }

  #
  # get service keytab
  #
  base::wallet { $svc_princ :
    path    => "${raddb}/radius.keytab",
    primary => true,
    owner   => 0,
    group   => $gid,
    mode    => '0640',
    require => File[ $raddb ],
  }

  #
  # get radius SSL key
  #
  base::wallet { "ssl-key/radius-${env}.stanford.edu":
    path    => "${raddb}/certs/radius.key",
    type    => 'file',
    owner   => 0,
    group   => $gid,
    mode    => '0640',
    require => File[ $raddb ],
  }

  #
  # create sql module
  #
  file { "${raddb}/mods-available/sql":
    owner   => 0,
    group   => $gid,
    mode    => '0640',
    content => template("${module_name}/etc/raddb/mods-available/sql.erb"),
    require => File[ $raddb ],
  }

  #
  # create ldap module
  #
  file { "${raddb}/mods-available/ldap":
    owner   => 0,
    group   => $gid,
    mode    => '0640',
    content => template("${module_name}/etc/raddb/mods-available/ldap.erb"),
    require => File[ $raddb ],
  }

  #
  # Allow connections to the RADIUS TCP/UDP ports (1812,1813).
  #
  base::iptables::rule { 'radius':
    ensure      => 'present',
    description => 'Allow radius connections',
    port        => [ '1812', '1813' ],
    protocol    => [ 'tcp', 'udp' ],
  }

  if ($enable_radsec) {
    #
    # Allow connections to the RADSEC TCP port (2083).
    #
    base::iptables::rule { 'radsec':
      ensure      => 'present',
      description => 'Allow radius connections',
      port        => [ '2083' ],
      protocol    => [ 'tcp' ],
    }
  }

  if ($backup_logs) {
    include radius::log_backup
  }

  #
  # Systemd setup
  #

  file { '/lib/systemd/system/radiusd.service':
    ensure  => file,
    content => template("${module_name}/lib/systemd/system/radiusd.service.erb"),
    owner   => 0,
    group   => 0,
    mode    => '0644',
  }

  service { 'radiusd':
    ensure  => 'running',
    enable  => true,
    require => File['/lib/systemd/system/radiusd.service'],
  }

}
