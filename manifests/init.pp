# UIT-ET-ACS RADIUS server configuration

class radius (
  $env            = undef,
) {

  # only prod and dev envs exist
  if ($env != 'dev' and $env != 'prod') {
    fail('Only prod and dev environments are supported')
  }

  # Support information.
  facts::instance {
    'su_group':       value => "radius-${env}";
    'su_munin_group': value => 'radius';
    'su_sysadmin0':   value => 'swl';
    'su_sysadmin1':   value => 'psr123';
    'su_sysadmin2':   value => 'vivienwu';
  }

  Base::Postfix::Recipient<|title == 'root@stanford.edu'|> {
    ensure => 'alert-auth@monitoring.stanford.edu',
  }

  alternatives {
    'iptables':   path => '/usr/sbin/iptables-legacy';
    'ip6tables':  path => '/usr/sbin/ip6tables-legacy';
  }

  include defaults
  include munin
  include package_docker
  include user::radius
  include su_local_root

  # Allow SSH from itlab SRWC subnet (rwc-itlab-itlab-pub-net)

  base::iptables::rule { 'itlab-22':
    ensure   => present,
    source   => [ '204.63.231.0/27' ],
    port     => [ '22' ],
    protocol =>  'tcp',
  }

  # For non-prod environments, rotate radius.log file.
  # Rotate logs into OLD logs directory
  base::newsyslog::config { 'radius':
    frequency    => 'daily',
    directory    => '/var/log/radius',
    archive      => '/afs/ir/service/auth/logs',
    restart      => 'run /usr/bin/docker exec radiusd.service /usr/sbin/radmin -e hup main.log',
    log_mode     => '644',
    log_owner    => 'radius',
    log_group    => 'radius',
    logs         => [ 'radius.log' ],
  }
  

  # extra users
  include radius::users
  include radius::root

  # FreeRADIUS configuration
  include radius::freeradius

  # Samba configuration
  include radius::samba

  # Duo Proxy configuration
  include radius::duoproxy

}
