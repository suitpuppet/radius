# Samba helper for RADIUS

class radius::samba (
  $env            = $radius::env,
  $docker_base    = 'us-docker.pkg.dev/uit-authnz/docker-public',
  $docker_tag     = 'stable',
  $wallet_base    = 'config/its-idg/radius',
  $uid            = 0,
  $gid            = 0,
  $samba_cache    = '/var/cache/samba',
  $samba_etc      = '/etc/samba',
  $samba_lib      = '/var/lib/samba',
  $samba_log      = '/var/log/samba',
) {

  $netbios_name  = upcase($::hostname)
  $server_string = regsubst($netbios_name, '\d+$', '')

  #
  # TODO: can we run the samba container as non-root?
  #
  file {
    [
      $samba_cache,
      $samba_etc,
      $samba_lib,
      $samba_log,
    ]:
      ensure => directory,
      owner  => $uid,
      group  => $gid,
      mode   => '0755'
  }

  file { "${samba_lib}/private":
    ensure  => directory,
    owner   => $uid,
    group   => $gid,
    mode    => '0700',
    require => File[$samba_lib],
  }

  #
  # Config
  #

  file { "${samba_etc}/smb.conf":
    ensure  => present,
    owner   => $uid,
    group   => $gid,
    mode    => '0644',
    content => template("${module_name}/etc/samba/smb.conf.erb"),
  }


  #
  # Credentials
  #

#  base::wallet { "${wallet_base}/samba-keytab-${::hostname}":
#    path  => "${samba_etc}/keytab",
#    type  => 'file',
#    owner => 0,
#    group => 0,
#    mode  => '0600',
#  }

  base::wallet { "${wallet_base}/samba-netlogon-creds-cli-tdb-${::hostname}":
    path  => "${samba_lib}/private/netlogon_creds_cli.tdb",
    type  => 'file',
    owner => 0,
    group => 0,
    mode  => '0600',
  }

#  base::wallet { "${wallet_base}/samba-secrets-ldb-${::hostname}":
#    path  => "${samba_lib}/private/secrets.ldb",
#    type  => 'file',
#    owner => 0,
#    group => 0,
#    mode  => '0600',
#  }

  base::wallet { "${wallet_base}/samba-secrets-tdb-${::hostname}":
    path  => "${samba_lib}/private/secrets.tdb",
    type  => 'file',
    owner => 0,
    group => 0,
    mode  => '0600',
  }

  #
  # Systemd setup
  #

  file { '/lib/systemd/system/samba.service':
    ensure  => file,
    content => template("${module_name}/lib/systemd/system/samba.service.erb"),
    owner   => 0,
    group   => 0,
    mode    => '0644',
  }

  service { 'samba':
    ensure  => 'running',
    enable  => true,
    require => File['/lib/systemd/system/samba.service'],
  }

}
