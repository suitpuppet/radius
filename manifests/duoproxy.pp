# Duo Authentication Proxy configuration

class radius::duoproxy (
  $env            = $radius::env,
  $docker_base    = 'us-docker.pkg.dev/uit-authnz/docker-public',
  $docker_tag     = 'latest',
  $uid            = 0,
  $gid            = 0,
  $wallet_base    = 'config/its-idg/radius',
  $wallet_name    = "${wallet_base}/authproxy-cfg-${env}",
  $http_proxies   = [],
  $duo_dir        = '/etc/duoproxy',
  $enable_bigip   = true,
) {

  # Make a directory to hold all the Duo proxy configurations.
  file { $duo_dir:
    ensure => directory,
    owner  => $uid,
    group  => $gid,
    mode   => '0755',
  }

  $base_cfg = "${duo_dir}/base.cfg"

  if (empty($http_proxies)) {
    $base_cfg = "${duo_dir}/authproxy.cfg"
  }

  #
  # Get the base configuration from wallet
  #
  base::wallet { $wallet_name :
    ensure  => present,
    path    => $base_cfg,
    type    => 'file',
    owner   => $uid,
    group   => $gid,
    mode    => '0640',
    require => File[$duo_dir],
    notify  => Exec['Update authproxy.cfg'],
  }

  #
  # Create the http-proxy config files (if there are any http proxies)
  #
  if (!empty($http_proxies)) {

    $proxy_num = 1

    $defaults = {
      duo_dir     => $duo_dir,
      duo_server  => undef,
      port        => undef,
      description => '',
      iptables    => [],
    }


    $http_proxies.each | String $fname, Hash $value | {
      $merged_hash_val = $defaults + $value
      radius::duoproxy::httpproxy { $fname :
        *      => $merged_hash_val,
        notify => Exec['Update authproxy.cfg'],
      }
    }

    # Install the script and remctl configuration that shows the http-proxy
    # config.

    file { '/usr/sbin/show-http-proxy-config':
      ensure => present,
      source => "puppet:///modules/${module_name}/bin/show-http-proxy-config",
      mode   => '0755',
    }

    file { '/etc/remctl/conf.d/http-proxy-config':
      ensure => present,
      source => "puppet:///modules/${module_name}/etc/remctl/conf.d/http-proxy-config",
    }


    if ( $enable_bigip ) {

      # Install the bigip pool management helper
      lb::bigip { 'lb':
        ensure      => present,
        signal_port => 9080,
      }

      # We need to install incommon-addtrust-2020 so that the load-balancer
      # does not throw a fit. We ensure that the ca-certificates and openssl
      # packages are installed as these are dependencies of
      # apache::cert::packages::root.
      ensure_packages(['openssl', 'ca-certificates'], { ensure => 'present' })

      # Install some extra root certs to take care of the F5 BigIP
      # SOAP service which does not send the complete chain.
      include apache::cert::packages

    }
  }

  $newsyslog_config = "etc/newsyslog.daily/duoproxy-${env}"

  # Rotate authproxy logs and push them to the syslog server
  file { '/etc/newsyslog.daily/duoproxy':
    ensure => present,
    source => "puppet:///modules/${module_name}/etc/newsyslog.daily/duoproxy-${env}",
  }

  base::syslog::fragment { '50-duoproxy.conf':
    ensure => present,
    source => "puppet:///modules/${module_name}/etc/rsyslog.d/50-duoproxy.conf",
  }

  # Ignore some messages relating to the duoauthproxy service.
  file { '/etc/filter-syslog/duoproxy':
    ensure => present,
    source => "puppet:///modules/${module_name}/etc/filter-syslog/duoproxy",
  }

  exec { 'Update authproxy.cfg':
    cwd     => $duo_dir,
    command => '/bin/cat base.cfg http-proxy-* >authproxy.cfg',
    require => Base::Wallet[ $wallet_name ],
  }

  #
  # Systemd setup
  #

  file { '/lib/systemd/system/duoproxy.service':
    ensure  => file,
    content => template("${module_name}/lib/systemd/system/duoproxy.service.erb"),
    owner   => 0,
    group   => 0,
    mode    => '0644',
  }

  service { 'duoproxy':
    ensure  => 'running',
    enable  => true,
    require => [
      File['/lib/systemd/system/duoproxy.service'],
      Exec['Update authproxy.cfg'],
    ],
  }

}
